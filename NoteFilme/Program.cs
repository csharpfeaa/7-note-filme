﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteFilme
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] filme = { "Bourne", "007", "Hunger Games" };
            int[][] ratings = new int[filme.Length][];

            Console.WriteLine(ratings.Length);

            Console.WriteLine("Introduceti note pentru urmatoarele filme. Un numar pentru nota sau 'next' pentru a trece la urmatorul film");

            // preluam notele filmului
            for (int film = 0; film < ratings.Length; film++)
            {
                ratings[film] = new int[0];
                bool intorduRating = true;
                int indexRating = 0;

                Console.WriteLine();
                Console.WriteLine(filme[film]);
                Console.WriteLine("---");

                while (intorduRating)
                {
                    Console.Write("Nota: ");
                    string textIntrodus = Console.ReadLine();

                    // daca sirul de caractere introdus este gol sau "next", trecem la urmatorul film
                    if (textIntrodus == "" || textIntrodus == "next")
                    {
                        intorduRating = false;
                    }
                    else
                    {
                        int nota;

                        // adaugam doar notele valide
                        if(!int.TryParse(textIntrodus, out nota) || nota < 0 || nota > 10)
                        {
                            Console.WriteLine("Nota nu este valida!");
                        }
                        else
                        {
                            Array.Resize(ref ratings[film], ratings[film].Length + 1);
                            ratings[film][indexRating] = nota;
                            indexRating++;
                        }
                    }
                }
            }

            Console.WriteLine();

            Console.WriteLine("ratings.Length: " + ratings.Length);

            // calculam ratingul pentru fiecare film
            for (int film = 0; film < ratings.Length; film++)
            {
                double ratingFilm = 0;
                double totalPuncte = 0;

                Console.WriteLine(filme[film] + " are " + ratings[film].Length + " voturi:");

                for (int vot = 0; vot < ratings[film].Length; vot++)
                {
                    String separator = vot < ratings[film].Length-1 ? ", " : "";
                    Console.Write(ratings[film][vot] + separator);

                    totalPuncte += ratings[film][vot];
                }

                ratingFilm = totalPuncte / ratings[film].Length;
                ratingFilm = Math.Round(ratingFilm, 2);

                Console.WriteLine();
                Console.WriteLine("Rating: " + ratingFilm);
                Console.WriteLine("---------");
                Console.WriteLine();
            }

            // filmul cu cele mai multe voturi
            Console.WriteLine();

            int indexMaxVoturi = 0;
            int nrMaxVoturi = 0;

            for (int i = 0; i < ratings.Length; i++)
            {
                if (nrMaxVoturi < ratings[i].Length)
                {
                    nrMaxVoturi = ratings[i].Length;
                    indexMaxVoturi = i;
                }
            }

            Console.WriteLine("Filmul cu cele mai multe voturi este: {0} si are {1} voturi.", filme[indexMaxVoturi], nrMaxVoturi);

            // filmul cu cel mai mare rating
            int indexRatingMare = 0;
            double ratingMare = 0;

            for (int i = 0; i < ratings.Length; i++)
            {
                double ratingFilm = 0;

                for (int j = 0; j< ratings[i].Length; j++)
                {
                    ratingFilm += ratings[i][j];
                }

                ratingFilm = ratingFilm / ratings[i].Length;

                if (ratingMare < ratingFilm)
                {
                    ratingMare = ratingFilm;
                    indexRatingMare = i;
                }
            }

            Console.WriteLine($"Filmul cu cel mai bun rating este {filme[indexRatingMare]} cu urmatorul rating {ratingMare}");

            Console.ReadKey();
        }
    }
}
